# RUQueue Python Client #

This is a Python class for communicating with the RUQueue ticketing system. It uses its XML API. Currently, only the LDAP authentication method is supported. 

A secret key auth mechanism is also available, but neither it nor the "Queue List" functionality is implemented in this client. Both of those would require a request for API access, and there is not currently a process in place for this (nor is there likely to ever be one).

![hdrt-api.png](https://bitbucket.org/repo/gGLnbg/images/1422060821-hdrt-api.png)
