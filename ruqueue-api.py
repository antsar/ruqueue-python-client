# RUQueue API Client Example
# See https://hdrt.rutgers.edu/api/RUQUEUE.dtd for the API's XML schema.
# This code can be improved quite a bit, its just a mockup to show the possible functionality.

from getpass import getpass

def main():
    """ This is an API demo method. It authenticates the user and 
    allows them to create a simple ticket. """
    
    # Initialize API
    api = RUQueueAPI(url = "https://hdrt-test.rutgers.edu")
    
    # Get username and password from user interactively, and login to the API
    username = raw_input("NetID: ")
    password = getpass("Password: ")
    if api.authenticate(username, password) is False:
        raise Exception("Invalid username of password. Please try again.")
    print "Welcome, %s! Lets create a ticket (Ctrl-C to abort)." % username
    queue = raw_input("Queue Name: ")
    user_netid = raw_input("User NetID: ")
    subject = raw_input("Ticket Subject: ")
    body = raw_input("Ticket Body: ")
    ticket_created = api.newticket(queue, user_netid, subject, body)
    if ticket_created is not False:
        print "Created ticket #%s.\n%s" % (ticket_created, api.base_url + "/ticket.php?id=" + ticket_created)
    else:
        print "The ticket was not created successfully."
    
class RUQueueAPI(object):
    
    def __init__(self, url):
        import sys
        sys.tracebacklimit = 0
        self.base_url = url
    
    # API Methods
    def authenticate(self, username, password):
        """ Authenticate user to the API using provided user,password tuple. """
        import base64
        import xml.etree.ElementTree as ET
        pass_b64 = base64.b64encode(password)
        xml_auth_request = self.xml_authenticate(username, pass_b64)
        xml_auth_response = self.xml_request(xml_auth_request)
        # parse XML
        xmlroot = ET.fromstring(xml_auth_response)
        auth_login = xmlroot.find('AUTH').find('AUTH_LOGIN')
        # Throw exception if username/password is bad
        if auth_login.get('status') == "102":
            # we can use auth_login.get('error_string') to get text of error here
            return False
        # If we have a session, save the session_key (enabling future transactions).
        auth_session = xmlroot.find('AUTH').find('AUTH_SESSION')
        if auth_session is not None:
            self.session_key = auth_session.get('session_key')
            self.username = auth_session.get('username')
            return True
        return False
        
    def newticket(self, queue, user_netid, subject, body):
        """ Create a ticket with the provided data. """
        import xml.etree.ElementTree as ET
        requester_email = self.username + "@rutgers.edu" # for now (HACK!)
        xml_newticket_request = self.xml_newticket(queue, subject, body, requester_email, user_netid)
        xml_newticket_response = self.xml_request(xml_newticket_request)
        # parse XML
        xmlroot = ET.fromstring(xml_newticket_response)
        ticket = xmlroot.find('TICKET')
        if ticket.get('status') == "0" and ticket.get('id') is not None:
            return ticket.get('id')
        return False
    
    def xml_request(self, xml):
        """ Perform an API request and return the resulting XML (string) """
        import requests
        headers = {'Content-Type': 'application/xml'}
        request_url = self.base_url + "/api/xml_api.php"
        reply = requests.post(request_url, data=xml, headers=headers)
        return reply.text
    
    # XML Generators
    # (these should be rewritten to build the XML tree rather than hard-coding it)
    def xml_authenticate(self, username, password_base64):
        return """<?xml version='1.0' encoding='UTF-8'?>
            <!DOCTYPE RUQUEUE SYSTEM "%s/api/RUQUEUE.dtd">
            <RUQUEUE>
              <AUTH keep_session="1">
                <AUTH_LOGIN method="LDAP" username="%s" password="%s" />
              </AUTH>
            </RUQUEUE>""" % (self.base_url, username, password_base64)
    
    def xml_newticket(self, queue, subject, body, requester_email, user_netid):
        if not self.session_key or not self.username:
            raise Exception("Session key is required for this operation")
        return """<?xml version='1.0' encoding='UTF-8'?>
            <!DOCTYPE RUQUEUE SYSTEM "%s/api/RUQUEUE.dtd">
            <RUQUEUE>
              <AUTH>
                 <AUTH_SESSION session_key="%s" username="%s" />
              </AUTH>
              <TICKET mode="create" queue="%s" subject="%s" requester="%s" user_netid="%s">
                <HISTORY>
                  <NEW_COMMENT subject="%s">
                    <![CDATA[%s]]>
                  </NEW_COMMENT>
                </HISTORY>
              </TICKET>
            </RUQUEUE>""" % (self.base_url, self.session_key, self.username, queue, subject, requester_email, user_netid, subject, body)
        
# run code if called directly
if __name__ == '__main__':
    main()